# DOAG Schulungstag 2016

Continuous Delivery ist eine Sammlung von Techniken, Prozessen und Werkzeugen, die den Softwareauslieferungsprozess in Sachen Schnelligkeit, Qualität und 
Durchgängigkeit deutlich verbessert. Dreh- und Angelpunkt ist dabei die Deployment-Pipeline. Sie beschreibt den Weg, den eine Software von der Entwicklung 
bis zur Veröffentlichung optimalerweise durchläuft.

In diesem Hands-on-Workshop erlernen die Teilnehmer den Aufbau einer effektiven Deployment-Pipeline für ihre Softwareprojekte, die mit der Oracle Fusion 
Middleware umgesetzt werden. Das umfasst sowohl den Einsatz von SOA/Integration als auch ADF. Für diese Technologien sind im Rahmen eines erfolgreichen 
Continuous Delivery bestimmte Voraussetzungen und Besonderheiten zu beachten, die jeder Entwickler kennen sollte. Darüber hinaus wird aufgezeigt, wie die Teilnehmer 
die Konzepte auch auf Cloud-Architekturen anwenden können.

Am Ende haben die Teilnehmer folgende Ziele erreicht:

* Sie wissen, wie sie einen ganzheitlichen Delivery Prozess mit der OFM aufsetzen
* Sie erfahren, ob sie besser auf on Premise oder auf die Cloud setzen
* Sie lernen git, Maven, Jenkins, SonarQube, Attlassian Tool Suite (Confluence, bitbucket, JIRA, Bamboo) gewinnbringend einzusetzen
* Sie können auf Projekttemplates zurückgreifen
* Sie wissen wie Qualitätsstandards eingehalten und Codeüberdeckungen gemessen werden
* Sie können ihr Know-how auf ADF- und SOA-Projekte (SOA Suite, OSB) anwenden

# Zielgruppe

"Oracle Fusion Middleware"-Entwickler (SOA/Integration, ADF), Architekten, generell Entwickler im Oracle-Umfeld

# Level

Die Teilnehmer haben im Idealfall bereits Erfahrungen mit der Oracle Fusion Middleware gesammelt und kennen die Entwicklungswerkzeuge (z. B. JDeveloper 12c). Ferner ist Erfahrung im Umgang mit Flyway, Apache Maven, Artifactory/Nexus, Jenkins/Hudson sinnvoll. Dies ist jedoch keine absolute Voraussetzung, da die Übungen unter Anleitung stattfinden.

