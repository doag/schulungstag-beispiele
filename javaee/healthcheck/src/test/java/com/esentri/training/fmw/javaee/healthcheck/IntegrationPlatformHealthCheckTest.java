package com.esentri.training.fmw.javaee.healthcheck;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

/**
 * <p>
 * A test case for <code>IntegrationPlatformCheckService</code>.</p>
 *
 * @author esentri AG, <a href="mailto:markus.lohn@esentri.com">Markus Lohn</a>
 */
public class IntegrationPlatformHealthCheckTest {

    public IntegrationPlatformHealthCheckTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Execute a platform check against an environment configured by
     * jndi.properties. The test case uses always RMI as protocol! However keep
     * in mind to configure HTTP as protocol in jndi.properties.
     *
     * Note: In case of an exception the test will not fail! It is not intended for
     * a automated test.
     */
    //@Test
    public void testCheck() {
        try {
            HealthCheck checkService = new HealthCheck();
            boolean ok = checkService.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
