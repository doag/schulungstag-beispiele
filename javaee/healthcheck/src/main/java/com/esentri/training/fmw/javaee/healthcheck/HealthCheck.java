package com.esentri.training.fmw.javaee.healthcheck;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeDataSupport;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * It runs checks against Oracle integration products (OSB and SOA Suite) to
 * verify that all required services and artifacts are up and running. It uses
 * exposed MBeans from this products to do the verification.</p>
 * <p>
 * In OSB it looks up Proxy Services and verifies the attribute "enabled=true".
 * In SOA Suite it looks up Composites and verifies the attributes "mode=active"
 * and "state=on". </p>
 *
 * @author esentri AG, <a href="mailto:markus.lohn@esentri.com">Markus Lohn</a>
 */
public final class HealthCheck {

    /**
     * The number of expected OSB projects.
     */
    private static final int DEFAULT_NUMBER_OF_OSB_PROJECTS = 3;

    /**
     * The name of the RuntimeService MBEAN provided by WebLogic Server. This
     * bean is used to retrieve information about the current server.
     */
    private static final String RUNTIMESERVICEBEAN_NAME =
        "com.bea:Name=RuntimeService,Type=weblogic.management.mbeanservers.runtime.RuntimeServiceMBean";

    /**
     * This logger is only used to print the fault details into a separated log
     * file.
     */
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * The connection to the MBean server.
     */
    private MBeanServerConnection connection;

    /**
     * Indicates SOA Suite was installed.
     */
    private boolean soaInstalled = false;

    /**
     * Indicates OSB was installed.
     */
    private boolean osbInstalled = false;

    /**
     * A List with report items build when executing a check.
     */
    private List<HealthCheckReportItem> reportItems = new ArrayList<HealthCheckReportItem>();

    /**
     * Creates a new <code>IntegrationPlatfromCheckService</code>.
     *
     * @param properties the configuration settings for this service.
     */
    public HealthCheck(Map properties) throws Exception {
    }

    /**
     * Creates a new <code>IntegrationPlatfromCheckService</code> with default
     * settings.
     */
    public HealthCheck() throws Exception {
    }

    public boolean isConnected() {
        return (connection != null);
    }

    public void disconnect() {
        connection = null;
    }

    public void connect() {
        logger.info("Connects to WebLogic Server...");

        if (connection == null) {
            try {
                connection = getLocalMBeanServer();
                lookupOSBInstallation();
                lookupSOAInstallation();
            } catch (Exception ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }
    }

    /**
     * Indicates SOA Suite is available on this server.
     *
     * @return true = SOA Suite installed otherwise false
     */
    public boolean isSOAInstalled() {
        return (isConnected() && soaInstalled);
    }

    /**
     * Indicates OSB is available on this server.
     *
     * @return true = OSB installed otherwise false
     */
    public boolean isOSBInstaled() {
        return (isConnected() && osbInstalled);
    }

    /**
     * Gets a List with HealthCheckReportItem objects. This list will be rebuild
     * every time when a check gets executed.
     * @return a List with HealthCheckReportItem
     */
    public List<HealthCheckReportItem> getReport() {
        return reportItems;
    }

    /**
     * Runs the checks against OSB and SOA Suite.
     *
     * @return true if everything working as expected, otherwise false
     * @throws Exception if operation failed
     * @throws com.esentri.jee.commons.ConnectRuntimeException if connection to
     * MBean server could not be established
     */
    public boolean execute() throws Exception {
        reportItems.clear();
        boolean ok = false;

        connect();

        try {
            ok = (checkSOA() && checkOSB());
        } finally {
            disconnect();
        }
        return ok;
    }

    /**
     * Checks whether Proxy Services in OSB where the name begins with
     * "mm2psipenta".
     *
     * @return true = projets exists otherwise false
     * @throws Exception if operation failed
     */
    private boolean checkOSB() throws Exception {
        boolean ok = true;
        if (isOSBInstaled()) {
            logger.debug("OSB is installed on this server. Check for the required projects...");
            ObjectName name =
                new ObjectName("com.oracle.osb:Name=ProxyService$mm2psipenta*," + "Type=ResourceConfigurationMBean");
            logger.trace("Lookup MBeans with ObjectName= {}.", name);
            Set<ObjectInstance> proxyServices = connection.queryMBeans(name, null);
            if (proxyServices != null && proxyServices.size() >= DEFAULT_NUMBER_OF_OSB_PROJECTS) {
                for (ObjectInstance proxyService : proxyServices) {
                    boolean enabled = checkProxyService(proxyService);
                    if (!enabled) {
                        ok = false;
                    }
                }
            } else {
                ok = false;
            }

            logger.debug("Done.");
        } else {
            logger.debug("OSB is not installed on this server.");
        }
        return ok;
    }

    /**
     * Checks whether an OSB Proxy Service is enabled or not.
     *
     * @param proxyService the Proxy Service to check
     * @return true = enabled otherwise false
     * @throws MBeanException ObjectName of the Proxy Service could not be
     * determined
     * @throws Exception if operations failed
     */
    private boolean checkProxyService(ObjectInstance proxyService) throws Exception {
        if (logger.isTraceEnabled()) {
            printObjectInstance(proxyService);
        }
        CompositeDataSupport configuration =
            (CompositeDataSupport) connection.getAttribute(proxyService.getObjectName(), "Configuration");
        CompositeDataSupport operationalSettings = (CompositeDataSupport) configuration.get("operational-settings");
        boolean enabled = ((Boolean) operationalSettings.get("enabled")).booleanValue();
        logger.debug("ProxyService {}. Enabled= {}", proxyService.getObjectName().getCanonicalName(), enabled);

        reportItems.add(new HealthCheckReportItem(proxyService.getObjectName().getCanonicalName(), "OSB ProxyService",
                                                  enabled));

        return enabled;
    }

    /**
     * Checks that the status and mode for the needed composites when running on
     * a SOA enabled server. When multiple versions of a composite available it
     * verifies that at least one of the composites have mode=active and
     * state=on.
     *
     * @return true = everything is up and running otherwise false
     * @throws Exception if operation failed
     */
    private boolean checkSOA() throws Exception {
        boolean ok = false;

        if (isSOAInstalled()) {
            logger.debug("SOA is installed on this server. Check for the required composites...");
            ObjectName name = new ObjectName("oracle.soa.config:*,name=\"mm2psipenta-freigabe\"");
            logger.trace("ObjectName= {}.", name);
            Set<ObjectInstance> composites = connection.queryMBeans(name, null);
            for (ObjectInstance composite : composites) {
                ok = checkComposite(composite);
            }
            logger.debug("Done.");
        } else {
            logger.debug("SOA is not installed on this server.");
            ok = true;
        }

        return ok;
    }

    /**
     * Checks whether a Composite is enabled or not.
     *
     * @param composite the Composite to check
     * @return true = enabled otherwise false
     * @throws Exception if operations failed
     */
    public boolean checkComposite(ObjectInstance composite) throws Exception {
        if (logger.isTraceEnabled()) {
            printObjectInstance(composite);
        }
        String mode = (String) connection.getAttribute(composite.getObjectName(), "Mode");
        String state = (String) connection.getAttribute(composite.getObjectName(), "State");
        boolean enabled = ("active".equalsIgnoreCase(mode) && "on".equalsIgnoreCase(state));
        logger.debug("Composite {}. Enabled= {}", composite.getObjectName().getCanonicalName(), enabled);

        reportItems.add(new HealthCheckReportItem(composite.getObjectName().getCanonicalName(), "SOA Suite", enabled));

        return enabled;
    }

    /**
     * Lookup SOA Suite installation.
     *
     * @throws MalformedObjectNameException if SOA Suite name is incorrect
     * @throws IOException if operation failed
     */
    public void lookupSOAInstallation() throws MalformedObjectNameException, IOException {
        soaInstalled = false;
        ObjectName name = new ObjectName("oracle.soa.config:*,name=fabric,type=FabricRuntime,Application=soa-infra");
        logger.trace("ObjectName= {}.", name);
        Set<ObjectInstance> mbeans = connection.queryMBeans(name, null);
        if (mbeans != null && mbeans.size() > 0) {
            soaInstalled = true;
        }
    }

    /**
     * Lookup OSB installation on the current server.
     *
     * @throws MalformedObjectNameException if OSB name is incorrect
     * @throws IOException if operation failed
     */
    private void lookupOSBInstallation() throws MalformedObjectNameException, IOException {
        osbInstalled = false;
        ObjectName name = new ObjectName("com.oracle.osb:Name=OSBConfigurationMBean,*");
        logger.trace("ObjectName= {}.", name);
        Set<ObjectInstance> mbeans = connection.queryMBeans(name, null);
        if (mbeans != null && mbeans.size() > 0) {
            osbInstalled = true;
        }
    }

    /**
     * Creates a connection to the local MBean server.
     *
     * @return the established connection
     * @throws ConnectRuntimeException if connection could not be established
     */
    private MBeanServerConnection getLocalMBeanServer() {
        Context context = getConnection();
        if (context == null) {
            throw new RuntimeException("InitialContext was not established.");
        }

        try {
            logger.trace("Lookup MBeanServer for this Managed Server...");
            MBeanServer mBeanServer = (MBeanServer) context.lookup("java:comp/env/jmx/runtime");
            logger.trace("Done.");
            return mBeanServer;
        } catch (Throwable t) {
            throw new RuntimeException(t.getMessage());
        }
    }

    /**
     * Write details about a MBean to the logfile.
     *
     * @param obj the MBean
     */
    private void printObjectInstance(ObjectInstance obj) {
        try {
            logger.info("------------------------------");
            logger.info("MBean: {}", obj.getObjectName());
            MBeanInfo info = connection.getMBeanInfo(obj.getObjectName());
            MBeanAttributeInfo[] attrInfos = info.getAttributes();
            for (int i = 0; i < attrInfos.length; i++) {
                String attrName = attrInfos[i].getName();
                Object attrValue = connection.getAttribute(obj.getObjectName(), attrName);
                logger.info("Name: {}, Value: {}", attrName, attrValue);
            }
        } catch (Exception ex) {
            logger.warn("Could print details about MBean {}, because of {}.", obj.toString(), ex.getMessage());
        }
    }

    /**
     * Normally another class provides the service to connect to the JNDI Context. In this case
     * only implemented to avoid dependencies.
     * @return always null
     */
    private Context getConnection() {
        logger.info("Connect");

        InitialContext ctx = null;
        try {
            ctx = new InitialContext();
            logger.info("Successfully connected to WebLogic Server.");
        } catch (NamingException ex) {
            throw new RuntimeException("Could not establish a connection to Weblogic Server.", ex);
        }
        return ctx;
    }

}
