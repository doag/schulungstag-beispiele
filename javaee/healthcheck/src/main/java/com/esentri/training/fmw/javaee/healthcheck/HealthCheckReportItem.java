package com.esentri.training.fmw.javaee.healthcheck;

import java.util.Objects;

/**
 * <p>
 * Represents a report for one verified object.</p>
 *
 * @author esentri AG, <a href="mailto:markus.lohn@esentri.com">Markus Lohn</a>
 */
public final class HealthCheckReportItem {

    /**
     * Creates a default HealthCheckReportItem.
     */
    public HealthCheckReportItem() {

    }

    /**
     * Creates a new HealthCheckReportItem.
     *
     * @param objectName the name of the object verified
     * @param type the type
     * @param active the status of the object as boolean value
     */
    public HealthCheckReportItem(String objectName, String type, boolean active) {
        this.objectName = objectName;
        this.type = type;
        this.active = active;
    }
    /**
     * The name of the verified object.
     */
    private String objectName;

    /**
     * Type of the verified object.
     */
    private String type;

    /**
     * Indicates whether the object is active.
     */
    private boolean active = false;

    /**
     * The hash code for this object.
     */
    private int hashCode = 0;

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        if (hashCode == 0) {
            hashCode = 3;
            hashCode = 83 * hashCode + Objects.hashCode(this.objectName);
            hashCode = 83 * hashCode + Objects.hashCode(this.type);
        }
        return hashCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HealthCheckReportItem other = (HealthCheckReportItem) obj;
        if (!Objects.equals(this.objectName, other.objectName)) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HealthCheckReportItem{" + "objectName=" + objectName + ", type=" + type + ", active=" + active + '}';
    }

}
