package com.esentri.training.fmw.javaee.healthcheck;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.naming.Context;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * It is responsible for running tests against Oracle Integration Products. When
 * all OSB and SOA Suite artefacts available, up and running than HTTP code 200
 * will be returned otherwise HTTP code 404. In cases of an exception HTTP code
 * 500 returned to the client.</p>
 *
 * @author esentri AG, <a href="mailto:markus.lohn@esentri.com">Markus Lohn</a>
 */
public class HealthCheckServlet extends HttpServlet {

    /**
     * This logger is only used to print the fault details into a separated log
     * file.
     */
    private final Logger logger = LoggerFactory.getLogger(getClass().getName());

    /**
     * Name and version information for this servlet.
     */
    private String info = null;

    /**
     * The health check implementation.
     */
    private HealthCheck healthcheck = null;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);

        logger.info("Initializes HealthCheckServlet...");
        try {
            Map environment = getEnvironment(config);
            healthcheck = new HealthCheck(environment);
            healthcheck.connect();
        } catch (Throwable t) {
            logger.error("IntegrationPlatfromHealthCheck could not be prepared.", t);
            throw new ServletException(t);
        }

        VersionInfo vinfo = new VersionInfo();
        StringBuilder sb = new StringBuilder();
        sb.append("HealthCheckServlet");
        sb.append(" - ");
        sb.append(vinfo.getVersionNumberString());
        info = sb.toString();

        logger.info("Successfully initialized HealthCheckServlet.");
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {
            boolean ok = healthcheck.execute();

            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            try {
                out.print(buildHTMLReport(ok, healthcheck.getReport()));
             } finally {
                out.close();
            }

            if (ok) {
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
        } catch (Exception ex) {
            logger.error("Could not execute health check.", ex);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public void destroy() {
        super.destroy();
        healthcheck.disconnect();
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return info;
    }

    /**
     * Prepares a Map with environment settings.
     *
     * @param config the servlet configuration
     * @return a Map with environment settings
     */
    private Map getEnvironment(ServletConfig config) {
        String username = config.getInitParameter("principal");
        String password = username + "#1";
        Map environment = new HashMap(2);
        environment.put(Context.SECURITY_PRINCIPAL, username);
        environment.put(Context.SECURITY_CREDENTIALS, password);
        return environment;
    }

    /**
     * Builds a simple HTML report with the result of an executed check
     * @param active the overall status of the check
     * @param reportItems the generated report 
     * @return String containing the HTML report
     */
    private String buildHTMLReport(boolean ok, List<HealthCheckReportItem> reportItems) {
        StringBuilder sb = new StringBuilder();
        sb.append("<!DOCTYPE html>").append("\n");
        sb.append("<html>").append("\n");
        sb.append("<head>").append("\n");
        sb.append("<title>Health Check</title>").append("\n");
        sb.append("</head>").append("\n");
        sb.append("<body>").append("\n");
        sb.append("<p>Healt Check executed with status active=<b>").append(ok).append("</b></p>").append("\n");
        sb.append("<p>").append("\n");
        sb.append("<table>").append("\n");
        sb.append("<th>").append("Name").append("</th>").append("\n");
        sb.append("<th>").append("Type").append("</th>").append("\n");
        sb.append("<th>").append("Active").append("</th>").append("\n");
        for (HealthCheckReportItem reportItem : reportItems) {
            sb.append("<tr>").append("\n");
            sb.append("<td>").append(reportItem.getObjectName()).append("</td>").append("\n");
            sb.append("<td>").append(reportItem.getType()).append("</td>").append("\n");
            sb.append("<td>").append(reportItem.isActive()).append("</td>").append("\n");
            sb.append("</tr>").append("\n");
        }
        sb.append("</table>").append("\n");
        sb.append("</p>").append("\n");
        sb.append("</body>").append("\n");
        sb.append("</html>").append("\n");
        return sb.toString();
    }
}
