<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ page contentType="text/html;charset=windows-1252"%>
<%@ page import="com.esentri.training.fmw.javaee.healthcheck.VersionInfo"%>

<%
  VersionInfo versionInfo = new VersionInfo();
  String versionInfoString  = versionInfo.toHtml();
%>

<html lang="de">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"></meta>
    <title>Health Check</title>
  </head>
  <body>
      <h2>Health Check</h2>     
      <p>
        Version: <br>
        <%= versionInfoString%>
      </p>
     
    <h4>Funktionen:</h4>
    <p>
      <a href="logger.jsp">Logger konfigurieren...</a>
    </p>
     <p>
      <a href="<%=request.getContextPath()%>/execute">Health Check ausf&uuml;hren...</a>
    </p>
     
  </body>
</html>