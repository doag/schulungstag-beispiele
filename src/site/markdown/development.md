# Entwicklungshandbuch

In diesem Handbuch werden alle relevanten Informationen zur Entwicklung bei der RhB zusammengefasst. Für die Entwicklung steht eine
VM zur Verfügung, die mit vagrant verwaltet wird. Die Konfiguration der VM ist ebenfalls in der Sourcecodeverwaltung in gitbucket enthalten.


## Konventionen

Die folgenden Konventionen finden in diesem Handbuch Anwendung:

**ORACLE_HOME** 
ORACLE_HOME is a system variable, which holds a path to current Oracle software installation folders. The default from Oracle 
follows /u01/app/oracle/product/[version]/[product name].

**GIT_ROOT**
Basisverzeichnis in dem der Sourcecode ausgecheckt wurde.


## Development VM

Die VM nutzt Ubuntu OS und enthält die folgenden Verzeichnisse:

    ORACLE_HOME = /u01/app/oracle/fmw/jdev-soa-12130


**Note:** Always replace the value of the variables in the following scripts!

## VM

1. Install Oracle Virtual Box on the local desktop PC. [Documentation](http://www.oracle.com/technetwork/server-storage/virtualbox/documentation/index.html), [Download](http://www.oracle.com/technetwork/server-storage/virtualbox/downloads/index.html)

2. Install Vagrant on the local desktop PC. [Documentation](https://www.vagrantup.com/docs/), [Download](https://www.vagrantup.com/downloads.html)

3. Startup the development environment based on the [Readme.md](http://www.test/)

## Checkout Source Code

The source code of the project is maintained in bitbucket, a gi repository. Use the following command in a shell to checkout the source code.

    #> git clone https://username@git.esentri.com/scm/rhb/rhb-integration.git


Example:

    #> git clone https://markus.lohn%40esentri.com@git.esentri.com/scm/rhb/rhb-integration.git


## Clean, Compile, Package and Deploy

### Clean

    #> mvn clean

### Compile

    #> mvn compile -DoracleHome=/u01/app/oracle/fmw/jdev-soa-12130

### Package

    #> mvn package -DoracleHome=/u01/app/oracle/fmw/jdev-soa-12130

### Deploy

There exists Maven proiles for dev,int and prod enviroment. When deploymnt is needed on dev use the followig command

    #> mvn pre-integration-test -Pdev -DoracleHome=/u01/app/oracle/fmw/jdev-soa-12130

### Test

    #> mvn integration-test -Pdev

   


