
import time
import re
import sys
import getopt

adminURL = None

try:
    opts, args = getopt.getopt(sys.argv[1:], "", ["username=", "password=", "adminURL"])
    for o, a in opts:
        if o == "--username":
            username = a
        elif o == "--password":
            password = a
        elif o == "--adminURL":
            adminURL = a
        else:
            assert False, "unhandled option"
except getopt.GetoptError, err:
       print str(err)  
       sys.exit(2)              
                
if adminURL == None:
  adminURL = 't3://localhost:7001'

serverName = 'AdminServer'
domain = 'compact_domain'

uniqueString = ''
appName = 'SAPAdapter'
moduleOverrideName = appName + '.rar'
moduleDescriptorName = 'META-INF/weblogic-ra.xml'


def makeDeploymentPlanVariable(wlstPlan, name, value, xpath, origin='planbased'):

  try:
       variableAssignment = wlstPlan.createVariableAssignment(name, moduleOverrideName, moduleDescriptorName)
       variableAssignment.setXpath(xpath)
       variableAssignment.setOrigin(origin)
       wlstPlan.createVariable(name, value)

  except:
    print('--> was not able to create deployment plan variables successfully')


def main():

  uniqueString = str(int(time.time()))

  try:
    print('--> about to connect to weblogic', adminURL)
    connect(username, password, adminURL)

    print('--> about getting metadata from SAPAdapter')
    edit() 
    startEdit()
    planPath = get('/AppDeployments/SAPAdapter/PlanPath')
    appPath = get('/AppDeployments/SAPAdapter/SourcePath')
    
    if planPath == None:
       planPath = '/u01/app/oracle/product/12.2.1.1/soaquickstart/soa/soa/SAPAdapterPlan.xml'

    print('--> Using plan ' + planPath)
    plan = loadApplication(appPath, planPath)
    print('--> adding variables to plan')
    uniqueString = uniqueString + '1'
    makeDeploymentPlanVariable(plan, 'ConnectionInstance_eis/SAPAdapter/SAP_JNDIName_' + uniqueString, 'eis/SAP/EDI/ZT030', '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + 'eis/SAP/EDI/ZT030' + '"]/jndi-name')
    uniqueString = uniqueString + '2'
    makeDeploymentPlanVariable(plan, 'ConnectionInstance_eis/SAPAdapter/SAP_JNDIName_' + uniqueString, 'eis/SAP/EDI/ZT031', '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + 'eis/SAP/EDI/ZT031' + '"]/jndi-name')
    uniqueString = uniqueString + '3'   
    makeDeploymentPlanVariable(plan, 'ConnectionInstance_eis/SAPAdapter/SAP_JNDIName_' + uniqueString, 'eis/SAP/EDI/TR003', '/weblogic-connector/outbound-resource-adapter/connection-definition-group/[connection-factory-interface="javax.resource.cci.ConnectionFactory"]/connection-instance/[jndi-name="' + 'eis/SAP/EDI/TR003' + '"]/jndi-name')
    print('--> saving plan')
    plan.save();
    save();
    print('--> activating changes')
    activate(block='true');
    cd('/AppDeployments/SAPAdapter/Targets');
    print('--> redeploying the SAPAdapter')
    redeploy(appName, planPath, targets=cmo.getTargets());
    print('--> done')

  except Exception, e :
    print('--> something went wrong, bailing out')
    print e 
    stopEdit('y')
    raise SystemExit

  print('--> disconnecting from admin server now')
  disconnect()

main()


